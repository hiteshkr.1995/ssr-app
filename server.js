require('./config/globals')

const PORT = 8000
const express = require('express')
var path = require('path')
var fs = require('fs')

/*Connect to NextJs*/
const next = require('next')
const dev = process.env.NODE_ENV !== 'production'
global.app = next({ dir: './resources', dev })
const handle = app.getRequestHandler()

multer  = require('multer')
upload = multer({ dest: 'uploads/' })
// var storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//     	cb(null, 'uploads')
//     },
//     filename: (req, file, cb) => {
//     	cb(null, file.fieldname + '-' + Date.now())
//     }
// });
// upload = multer({storage: storage})
const bodyParser = require('body-parser')
const session = require('express-session')
const cookieParser = require('cookie-parser')
passport = require('passport')

use('config/passport')
use('config/database')

const routes = use("routes")

app.prepare().then(() => {

	const server = express()

	// for parsing application/json
	server.use(bodyParser.json())

	// for parsing application/xwww-
	server.use(bodyParser.urlencoded({ extended: true }))
	//form-urlencoded

	// for parsing multipart/form-data
	// server.use(upload.array())

	server.use(session({
		name: 'framework_session',
		secret: 'keyboard cat',
		resave: false,
		saveUninitialized: false,
		// cookie: { secure: true }
	}))

	server.use(passport.initialize());
	server.use(passport.session());

	// Apply routes from the "routes" folder 
	server.use("/", routes)

	/* Error handling from async / await functions */
	server.use((err, req, res, next) => {
		const { status = 500, message } = err
		res.status(status).json(message)
	})

	server.get('*', (req, res) => {
		return handle(req, res)
	})

	server.listen(PORT, err => {
		if (err) throw err
		console.log(`> Ready on http://localhost: ${PORT}`)
	})

}).catch(ex => {

	console.error(ex.stack)
	process.exit(1)

})