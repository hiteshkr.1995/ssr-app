'use strict'
import React from 'react'
import Layout from 'components/Layout'

class AboutPage extends React.Component {
	render() {
		return (
			<Layout title="About Us" auth={this.props.auth}>
				<h1>This is about Page</h1>
			</Layout>
		)
	}
}

export default AboutPage