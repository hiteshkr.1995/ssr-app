'use strict'
import React from 'react'
import { addPost } from 'api/posts'
import Layout from 'components/Layout'
import dynamic from 'next/dynamic'

const DragAndDrop = dynamic({
	loader: () => import('components/DragAndDrop.js'),
	loading: () => <p>Loading ...</p>,
	ssr: false
})

const CKEditor = dynamic({
	loader: () => import('components/CKEditor'),
	loading: () => <p>Loading ...</p>,
	ssr: false
})


class PostsPage extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			title: '',
			textarea: '',
			date: undefined,
			load: false,
			fileToUpload: undefined
		}

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	componentDidMount() {
		this.setState({ date: new Date().toLocaleTimeString() })
	}
	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	}

	handleSubmit(event) {

		event.preventDefault()
		this.setState({ load: true })


		// Create a test FormData object
		let formData = new FormData()
		formData.append('title', this.state.title)
		formData.append('textarea', this.state.textarea)
		formData.append('fileToUpload', this.state.fileToUpload)

		addPost(formData).then(function (response) {

			// handle success
			console.log(response.data)

		}).catch(function (error) {

			// handle error
			console.log(error)

		}).then( () => {

			this.setState({ load: false })

		})

	}

	render() {

		return (
			<Layout title='Posts' auth={this.props.auth}>
				<form className="mt-2" onSubmit={this.handleSubmit}>
					<div className="row my-5">
						<div className="col-md-6">
							<div className="form-group">
								<label htmlFor="exampleInputTitle">Title</label>
								<input type="text" value={this.state.title} onChange={this.handleChange} name="title" className="form-control" id="exampleInputTitle" aria-describedby="titleHelp" placeholder="Enter Title" />
							</div>
							<div className="form-group">
								<label htmlFor="editor">Example textarea</label>
								<CKEditor
									onChange={ ( event, editor ) => {
										const data = editor.getData();
										this.setState({
											textarea: data
										});
									} }
								/>
							</div>
						</div>
						<div className="col-md-6">
							<DragAndDrop handleComppredImage={ (value) => {
								this.setState({
									fileToUpload: value
								})
							}}/>
						</div>
					</div>
					<small>Current Time: { this.state.date === undefined ? "Loading..." : this.state.date }</small>
					<button type="submit" className="btn btn-primary float-right" disabled={this.state.load}>{ this.state.load ? 'Loading...' : 'Submit' }</button>
				</form>
			</Layout>

		)
	}

}

export default PostsPage