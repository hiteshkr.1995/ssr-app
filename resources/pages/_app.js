'use strict'
import React from 'react'
import ErrorPage from 'pages/_error';
import App, { Container } from 'next/app'
import NProgress from 'nprogress'
import Router from 'next/router'
import { check } from 'api/auth'

Router.events.on('routeChangeStart', url => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class Layout extends React.Component {
	render () {
		const { children } = this.props
		return <div className='layout'>{children}</div>
	}
}

export default class MyApp extends App {

	static async getInitialProps({ Component, ctx }) {

		let pageProps = {}
		let auth = {}
		let geustRoutes = ['/auth/signin', '/auth/signup']

		let errorCode

		if (process.browser) {
			errorCode = false
		} else {
			errorCode = ctx.res.statusCode > 200 ? ctx.res.statusCode : false
		}

		if (errorCode) {

			return { errorCode }

		} else {

			if (process.browser) {

				// Client Side

				let reqc = await check()
				auth= {
					status: reqc.data ? true : false,
					data: reqc.data
				}

				if (!auth.status) {

					if (!geustRoutes.includes(ctx.pathname)) {

						Router.push('/auth/signin')

					}

				} else {

					if (geustRoutes.includes(ctx.pathname)) {

						Router.push('/')

					}

				}

			} else {

				// Server Side

				auth = {
					status: ctx.req.user ? true : false,
					data: ctx.req.user
				}
				if (!auth.status) {

					if (!geustRoutes.includes(ctx.pathname)) {

						ctx.res.writeHead(301, {
							Location: '/auth/signin'
						})
						ctx.res.end()

					}

				} else {

					if (geustRoutes.includes(ctx.pathname)) {

						ctx.res.writeHead(301, {
							Location: '/'
						})
						ctx.res.end()

					}

				}

			}

			if(auth.status) {
				if (Component.getInitialProps) {
					pageProps = await Component.getInitialProps(ctx)
				}
			}


			return { pageProps, auth }

		}

	}

	render () {

		if (this.props.errorCode) {
			return <ErrorPage statusCode={this.props.errorCode} />;
		}

		let { Component, pageProps, auth } = this.props
		auth = { auth }
		return (
			<Container>
				<Layout>
					<Component {...pageProps} {...auth} />
				</Layout>
			</Container>
		)
	}
}