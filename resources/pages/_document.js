import Document, { Head, Main, NextScript } from "next/document"
import { ServerStyleSheet } from 'styled-components'
import ErrorPage from 'pages/_error'

export default class MyDocument extends Document {
	static async getInitialProps ( ctx ) {

		const errorCode = ctx.res.statusCode > 200 ? ctx.res.statusCode : false

		if (errorCode) {

			return { errorCode }

		} else {
			const sheet = new ServerStyleSheet()
			const page = ctx.renderPage(App => props => sheet.collectStyles(<App {...props} />))
			const styleTags = sheet.getStyleElement()
			return { ...page, styleTags }
		}
	}

	render() {

		if (this.props.errorCode) {
			return <ErrorPage statusCode={this.props.errorCode} />;
		} else {

			return (
				<html lang="en">
					<Head>
						<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossOrigin="anonymous"/>
						<link rel="stylesheet" href="/static/css/nprogress.min.css" />
						<link rel="stylesheet" href="/static/css/style.css" />
						{this.props.styleTags}
					</Head>
					<body>

						<Main />
						<NextScript />

						<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossOrigin="anonymous"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossOrigin="anonymous"></script>
						<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossOrigin="anonymous"></script>

					</body>
				</html>
			)
		}
	}
}