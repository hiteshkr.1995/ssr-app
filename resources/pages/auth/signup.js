import React from 'react'
import Layout from 'components/Layout'
import Link from 'next/link'
import Head from 'next/head'
import { addUser } from 'api/auth'

class SignUpPage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			name: '',
			email: '',
			password: '',
		}

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value
		});
	}

	handleSubmit = event => {
		event.preventDefault()

		let formData = new FormData()

		formData.append('name', this.state.name)
		formData.append('email', this.state.email)
		formData.append('password', this.state.password)

		addUser(formData).then(function (response) {
			// handle success
			window.location = '/'
		})
		.catch(function (error) {
			// handle error
			console.log(error)
		})
		.then( () => {
			console.log('error')
		})
	}

	render() {
		return (

			<Layout title='Sign Up' auth={this.props.auth}>
				<Head>
					<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
					<link rel="stylesheet" type="text/css" href="/static/css/signup.css" />
				</Head>
				<div className="row">
					<div className="col-lg-10 col-xl-9 mx-auto">
						<div className="card card-signin flex-row my-5">
							<div className="card-img-left d-none d-md-flex"></div>
							<div className="card-body">
								<h5 className="card-title text-center">Register</h5>
								<form className="form-signin" onSubmit={this.handleSubmit}>

									<div className="form-label-group">
										<input type="text" id="inputName" name="name" value={this.state.name} onChange={this.handleChange} className="form-control" placeholder="Name" required />
										<label htmlFor="inputName">Name</label>
									</div>

									<div className="form-label-group">
										<input type="email" id="inputEmail" name="email" value={this.state.email} onChange={this.handleChange} className="form-control" placeholder="Email address" required />
										<label htmlFor="inputEmail">Email address</label>
									</div>
								
									<hr />

									<div className="form-label-group">
										<input type="password" id="inputPassword" name="password" value={this.state.password} onChange={this.handleChange} className="form-control" placeholder="Password" required />
										<label htmlFor="inputPassword">Password</label>
									</div>

									<button className="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign Up</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</Layout>

		)
	}
}

export default SignUpPage