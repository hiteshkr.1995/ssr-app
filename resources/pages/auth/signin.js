import React from 'react'
import Layout from 'components/Layout'
import Link from 'next/link'
import Head from 'next/head'

class SignInPage extends React.Component {
	constructor(props) {
		super(props)
	}

	handleSubmit(event) {
		event.preventDefault()
	}

	render() {
		return (

			<Layout title='Sign In' auth={this.props.auth}>
				<Head>
					<link rel="stylesheet" href="/static/css/signin.css" />
				</Head>
				<div className="row no-gutter">
					<div className="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
					<div className="col-md-8 col-lg-6">
						<div className="login d-flex align-items-center py-5">
							<div className="container">
								<div className="row shadow-lg p-3">
									<div className="col-md-9 col-lg-8 mx-auto">
										<h3 className="login-heading mb-4">Login System!</h3>
										<form method="POST" action="/auth/signin">
											<div className="form-label-group">
												<input type="email" id="inputEmail" name="email" className="form-control" placeholder="Email address" required autoFocus />
												<label htmlFor="inputEmail">Email address</label>
											</div>

											<div className="form-label-group">
												<input type="password" id="inputPassword" name="password" className="form-control" placeholder="Password" minLength="6" required />
												<label htmlFor="inputPassword">Password</label>
											</div>

											<div className="custom-control custom-checkbox mb-3">
												<input type="checkbox" className="custom-control-input" id="customCheck1" />
												<label className="custom-control-label" htmlFor="customCheck1">Remember password</label>
											</div>
											<button className="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign in</button>
											<div className="text-center">
												<a className="small" href="">Forgot password?</a>
											</div>
										</form>
									</div>
								</div>
								<div className="text-center mt-2">
									<span className="text-uppercase font-weight-bold text-center">OR</span>
								</div>
								<div className="row mt-2">
									<a className="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold text-white" href="/auth/signup">Create your account</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</Layout>

		)
	}
}

export default SignInPage