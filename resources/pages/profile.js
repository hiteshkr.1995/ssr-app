'use strict'
import React from 'react'
import Layout from 'components/Layout'
import dynamic from 'next/dynamic'

const CroppieWrap = dynamic({
	loader: () => import('components/CroppieWrap.js'),
	loading: () => <p>Loading ...</p>,
	ssr: false
})

class ProfilePage extends React.Component {

	render() {

		const header = {
			backgroundImage: "url('https://images.pexels.com/photos/956981/milky-way-starry-sky-night-sky-star-956981.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940')",
		}
		return (

			<Layout title={this.props.auth.data.name} auth={this.props.auth}>

				<header style={header} className="py-5 mb-5 row">
					<div className="container h-100">
						<div className="row h-100 align-items-center">
							<div className="col-lg-4">
								<div className="p-5">
									<CroppieWrap />
								</div>
							</div>
							<div className="col-lg-8">
								<h1 className="display-4 text-white mt-5 mb-2">{ this.props.auth.data.name }</h1>
								<p className="lead mb-5 text-white-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non possimus ab labore provident mollitia. Id assumenda voluptate earum corporis facere quibusdam quisquam iste ipsa cumque unde nisi, totam quas ipsam.</p>
							</div>
						</div>
					</div>
				</header>

				<div className="row">

					<div className="col-md-8 mb-5">
						<h2>What We Do</h2>
						<hr />
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt neque tempore recusandae animi soluta quasi? Asperiores rem dolore eaque vel, porro, soluta unde debitis aliquam laboriosam. Repellat explicabo, maiores!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis optio neque consectetur consequatur magni in nisi, natus beatae quidem quam odit commodi ducimus totam eum, alias, adipisci nesciunt voluptate. Voluptatum.</p>
						<a className="btn btn-primary btn-lg" href="#">Call to Action &raquo;</a>
					</div>
					<div className="col-md-4 mb-5">
						<h2>Contact Us</h2>
						<hr/>
						<address>
							<strong>Start Bootstrap</strong>
							<br />3481 Melrose Place
							<br />Beverly Hills, CA 90210
							<br />
						</address>
						<address>
							<abbr title="Phone">P:</abbr>
							(123) 456-7890
							<br />
							<abbr title="Email">Email:</abbr>
							<a href="mailto:#">{this.props.auth.data.email}</a>
						</address>
					</div>

				</div>

				<div className="row">
					<div className="col-md-4 mb-5">
						<div className="card h-100">
							<img className="card-img-top" src="http://placehold.it/300x200" alt="" />
							<div className="card-body">
								<h4 className="card-title">Card title</h4>
								<p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque sequi doloribus.</p>
							</div>
							<div className="card-footer">
								<a href="#" className="btn btn-primary">Find Out More!</a>
							</div>
						</div>
					</div>
					<div className="col-md-4 mb-5">
						<div className="card h-100">
							<img className="card-img-top" src="http://placehold.it/300x200" alt="" />
							<div className="card-body">
								<h4 className="card-title">Card title</h4>
								<p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque sequi doloribus totam ut praesentium aut.</p>
							</div>
							<div className="card-footer">
								<a href="#" className="btn btn-primary">Find Out More!</a>
							</div>
						</div>
					</div>
					<div className="col-md-4 mb-5">
						<div className="card h-100">
							<img className="card-img-top" src="http://placehold.it/300x200" alt="" />
							<div className="card-body">
								<h4 className="card-title">Card title</h4>
								<p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>
							</div>
							<div className="card-footer">
								<a href="#" className="btn btn-primary">Find Out More!</a>
							</div>
						</div>
					</div>
				</div>

			</Layout>

		)
	}

}

export default ProfilePage