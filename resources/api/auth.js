import axios from 'axios'

export function addUser (data) {
	return axios.post('http://127.0.0.1:8000/auth/signup', data)
}

export function check (data) {
	return axios.post('http://127.0.0.1:8000/auth/check')
}