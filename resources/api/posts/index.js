import axios from 'axios'

export function addPost (data) {
	const config = {
		headers: {
			'content-type': 'multipart/form-data'
		}
	}
  return axios.post('http://127.0.0.1:8000/posts/add', data, config)
}