import React, { Component } from 'react'
import { CompressorLib } from 'static/js/main.js'

class DragAndDrop extends Component {

	state = {
		dragging: false,
		image: "",
		blobImage: {}
	}

	dropRef = React.createRef()
	fileToUpload = React.createRef()

	handleDrag = (e) => {
		e.preventDefault()
		e.stopPropagation()
	}
	handleDragIn = (e) => {
		e.preventDefault()
		e.stopPropagation()
		if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
			this.setState({dragging: true})
		}
	}
	handleDragOut = (e) => {
		e.preventDefault()
		e.stopPropagation()
		this.setState({dragging: false})
	}
	handleDrop = (e) => {
		e.preventDefault()
		e.stopPropagation()
		this.setState({dragging: false})
		if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
			this.getBase64(e.dataTransfer.files[0], (result) => {
				this.setState({
					image: result
				})
			})

		}
	}

	handleClick = (e) => {
		this.fileToUpload.current.click()
	}

	handleCallBack = (value) => {
		this.props.handleComppredImage(value)
	}

	getBase64(file, cb) {
		CompressorLib(file).then( (data) => {

			this.handleCallBack(data)

			let reader = new FileReader();
			reader.onload = (readerEvent) => {
				let value = readerEvent.target.result;
				cb(value)
			};
			reader.readAsDataURL(data);
		});
	}

	componentDidMount() {
		let div = this.dropRef.current
		div.addEventListener('dragenter', this.handleDragIn)
		div.addEventListener('dragleave', this.handleDragOut)
		div.addEventListener('dragover', this.handleDrag)
		div.addEventListener('drop', this.handleDrop)
		div.addEventListener('click', this.handleClick)
	}

	componentWillUnmount() {
		let div = this.dropRef.current
		div.removeEventListener('dragenter', this.handleDragIn)
		div.removeEventListener('dragleave', this.handleDragOut)
		div.removeEventListener('dragover', this.handleDrag)
		div.removeEventListener('drop', this.handleDrop)
		div.removeEventListener('click', this.handleClick)
	}

	handleChange = (e) => {
		this.getBase64(e.target.files[0], (result) => {
			this.setState({
				image: result
			})
		})
	}

	render() {

		let divStyle = {
			backgroundColor: this.state.dragging ? 'aliceblue' : '#fff',
			border:  this.state.dragging ? '0.25em dashed green' : '0.25em dashed silver',
			height: '100%',
			padding: '15px',
		}

		return (
			<React.Fragment>
				<div ref={this.dropRef} className="text-center" style={{ height: '380px', cursor: 'pointer' }}>
					<input type="file" ref={this.fileToUpload} onChange={this.handleChange} name="fileToUpload" accept="image/*" className="d-none" />
					<img src={this.state.image} className={"img-fluid" + (this.state.image.length > 0 ? '' : ' d-none')} style={{ height: '100%' }} />
					<div className={"align-items-center justify-content-center " + (this.state.image.length > 0 ? 'd-none' : 'd-flex')} style={divStyle}>
						<h2 className="m-0">Select or Drop Image Here</h2>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default DragAndDrop