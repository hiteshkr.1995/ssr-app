import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import NavBar from './NavBar'

export default ({ children, title = 'App Name', auth={status: false} }) => (
	<React.Fragment>
		<Head>
			<title>{title}</title>
		</Head>
		<NavBar auth={auth} />
		<div className="container-fluid">
			{children}
		</div>
	</React.Fragment>
)