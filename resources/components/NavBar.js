import React from 'react'
import Link from './Link'

class NavBar extends React.Component {

	render() {

		let signin = <a className="btn btn-outline-success my-2 font-weight-bold my-sm-0" href="/auth/signin">Sign In</a>
		let logout = <a className="btn btn-outline-success my-2 font-weight-bold my-sm-0" href="/logout">Logout</a>

		return (
			<nav style={{ zIndex: 1000 }} className="navbar sticky-top navbar-expand-lg navbar-light bg-light">
				<Link href="/">
					<a className="navbar-brand">APP</a>
				</Link>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>

				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav mr-auto">
						{
							this.props.auth.status &&
							<React.Fragment>
								<li className="nav-item">
									<Link activeClassName='active' href="/posts">
										<a className="nav-link">Posts</a>
									</Link>
								</li>
								<li className="nav-item">
									<Link activeClassName='active' href="/profile">
										<a className="nav-link">Profile</a>
									</Link>
								</li>
								<li className="nav-item">
									<Link activeClassName='active' href="/about">
										<a className="nav-link">About Us</a>
									</Link>
								</li>
							</React.Fragment>
						}
					</ul>
					<span>
						{this.props.auth.status ?
							<div className="btn-group">
								<button type="button" className="btn btn-outline-success font-weight-bold dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									{this.props.auth.data.name}
								</button>
								<div className="dropdown-menu dropdown-menu-right">
									<button className="dropdown-item" type="button">Action</button>
									<a className="dropdown-item" href="/logout">Logout</a>
								</div>
							</div> : signin
						}
					</span>
				</div>
			</nav>
		)

	}

}

export default NavBar