import React, { Component } from 'react'

import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from 'static/js/ckeditor/classic'
import MyCustomUploadAdapterPlugin from 'static/js/ckeditor/image-upload.js'
import 'static/js/imagecompressor.js'

class CKEditorWrapper extends Component {
	render() {
		return (
			<React.Fragment>
				<CKEditor
					editor = { ClassicEditor }
					data = ""
					onInit={ editor => {
						// You can store the "editor" and use when it is needed.
						console.log( 'Editor is ready to use!', editor );
					} }
					onBlur={ editor => {
						console.log( 'Blur.', editor );
					} }
					config={ {
						placeholder: 'Write your content...',
						extraPlugins: [ MyCustomUploadAdapterPlugin ],
					} }
					onFocus={ editor => {
						console.log( 'Focus.', editor );
					} }
					{...this.props}
				/>
			</React.Fragment>
		)
	}
}

export default CKEditorWrapper