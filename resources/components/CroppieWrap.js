import React, { Component } from 'react'
import Croppie from 'static/js/croppie/croppie.js'
import Head from 'next/head'


class CroppieWrap extends Component {

	componentDidMount() {
		this.setUpCroppie()
	}

	setUpCroppie() {
		let el = document.getElementById('cropp');
		let vanilla = new Croppie(el, {
			viewport: { width: 200, height: 200, type: 'circle' },
			boundary: { width: 250, height: 250 },
			showZoomer: false,
			enableOrientation: true
		});
		vanilla.bind({
			url: 'https://blackrockdigital.github.io/startbootstrap-one-page-wonder/img/01.jpg',
		})
		// .then(function() {
		// 	vanilla.result('blob').then(function(blob) {
		// 		console.log('blob', blob)
		// 	});
		// })

		el.addEventListener('update', (ev) =>  {
			vanilla.result('blob').then(function(blob) {
			});
		});
	}

	render() {
		return (
			<React.Fragment>
				<Head>
					<link rel="stylesheet" type="text/css" href="/static/css/croppie.css" />
				</Head>
				<div id="cropp" />
			</React.Fragment>
		)
	}

}

export default CroppieWrap