'use strict'
import React from 'react'
import Router from 'next/router'
import { check } from 'api/auth'

const Auth = (BaseComponent) => {

	class AuthClass extends React.Component {

		static async getInitialProps(ctx) {

			let auth = {}

			if (process.browser) {

				reqc = await check()

				auth = {
					success: reqc.data ? true : false,
					details: reqc.data
				}

			} else {

				auth = {
					success: ctx.req.user ? true : false,
					details: ctx.req.user
				}

			}

			return {auth}
		}

		render() {
			return <BaseComponent {...this.props} />
		}

	}

	return AuthClass

}


export default Auth