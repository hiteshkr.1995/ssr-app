class MyUploadAdapter {
	constructor( loader ) {
		/*The file loader instance to use during the upload.*/
		this.loader = loader;
		this.loader.file.then( (File) => {
			// this.File = File;
			this.File = compress(File).then( (data) => {
				return data;
			} );
		} );
	}

	/*Starts the upload process.*/
	upload() {
		return new Promise( ( resolve, reject ) => {
			this._initRequest();
			this._initListeners( resolve, reject );
			this._sendRequest();
		} );
	}

	/*Aborts the upload process.*/
	abort() {
		if ( this.xhr ) {
			this.xhr.abort();
		}
	}

	/*Initializes the XMLHttpRequest object using the URL passed to the constructor.*/
	_initRequest() {
		let xhr = this.xhr = new XMLHttpRequest();

		/*Note that your request may look different. It is up to you and your editor
		integration to choose the right communication channel. This example uses
		a POST request with JSON as a data structure but your configuration
		could be different.*/
		xhr.open( 'POST', UploadUrl, true );
		xhr.responseType = 'json';
	}

	/*Initializes XMLHttpRequest listeners.*/
	_initListeners( resolve, reject ) {
		const xhr = this.xhr;
		const loader = this.loader;
		const genericErrorText = 'Couldn\'t upload file:' + ` ${ this.File.name }.`;

		xhr.addEventListener( 'error', () => reject( genericErrorText ) );
		xhr.addEventListener( 'abort', () => reject() );
		xhr.addEventListener( 'load', () => {
			const response = xhr.response;

			/*This example assumes the XHR server's "response" object will come with
			an "error" which has its own "message" that can be passed to reject()
			in the upload promise.
			
			Your integration may handle upload errors in a different way so make sure
			it is done properly. The reject() function must be called when the upload fails.*/
			if ( !response || response.errors ) {
				return reject( response && response.errors ? response.errors.upload[0] : genericErrorText );
			}

			/*If the upload is successful, resolve the upload promise with an object containing
			at least the "default" URL, pointing to the image on the server.
			This URL will be used to display the image in the content. Learn more in the
			UploadAdapter#upload documentation.*/
			resolve( {
				default: response.url,
			} );
		} );

		/*Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
		properties which are used e.g. to display the upload progress bar in the editor
		user interface.*/
		if ( xhr.upload ) {
			xhr.upload.addEventListener( 'progress', evt => {
				if ( evt.lengthComputable ) {
					loader.uploadTotal = evt.total;
					loader.uploaded = evt.loaded;
				}
			} );
		}
	}

	/*Prepares the data and sends the request.*/
	_sendRequest() {
		this.File.then( (value) => {
			/*Prepare the form data.*/
			let data = new FormData();
			data.append( 'upload', value );

			/*Important note: This is the right place to implement security mechanisms
			like authentication and CSRF protection. For instance, you can use
			XMLHttpRequest.setRequestHeader() to set the request headers containing
			the CSRF token generated earlier by your application.

			Send the request.*/
			this.xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
			this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			this.xhr.send( data );
		});
	}
};

export default function MyCustomUploadAdapterPlugin( editor ) {
	editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
		/*Configure the URL to the upload script in your back-end here!*/
		return new MyUploadAdapter( loader );
	};
};