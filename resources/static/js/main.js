import ImageCompressor from 'static/js/imagecompressor.js'

export function CompressorLib(file) {
	return new Promise((resolve, reject) => {
		new ImageCompressor(file, {
			quality: .6,
			success(result) {
				resolve(result);
			},
			error(e) {
				reject(e);
			},
		 });
	});
};