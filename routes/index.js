'use strict'

const express = require("express")
const Route = express.Router()
const User = use("app/Models/User")

const AuthController = use("app/Http/Controller/Auth")
const PostController = use("app/Http/Controller/Post")

/* Error handler for async / await functions */
const catchErrors = fn => {
	return function(req, res, next) {
		return fn(req, res, next).catch(next)
	}
}

Route.post('/auth/signup', new AuthController().signup)
Route.post('/auth/signin',
	passport.authenticate('local', { failureRedirect: '/auth/signin' }),
	new AuthController().signin
)
Route.post('/posts/add', new PostController().index)
Route.post('/auth/check', new AuthController().check)
Route.get('/auth/check', new AuthController().check)

Route.get('/logout', function(req, res){
	req.logout()
	res.redirect('/')
})

Route.get('/test', function(req, res){
	// User.
})

module.exports = Route