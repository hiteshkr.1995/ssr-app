global.base_dir = process.cwd()
global.abs_path = function(path) {
	return base_dir + path
}

global.use = function(file) {
	return require(abs_path('/' + file))
}