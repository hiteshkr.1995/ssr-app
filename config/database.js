const mongoose = require('mongoose')

// Connect Mongodb
mongoose.connect('mongodb://tessAdmin:abc123@localhost:27017/next_js', {
	useNewUrlParser: true,
	useCreateIndex: true,
	useFindAndModify: false
}).then(() => console.log("Database connected."))

mongoose.connection.on("error", err => {
	console.log(`DB connection error: ${err.message}`)
})