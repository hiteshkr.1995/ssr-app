'use strict'
const localStrategy = require('passport-local').Strategy
const User = use('app/Models/User')

passport.serializeUser(function(user, done) {
	done(null, user._id)
})

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		done(null, user)
	})
})


passport.use(new localStrategy({
		usernameField: 'email',
		passwordField: 'password'
	},
	function(username, password, done){

		User.findOne({email: username}).select("+password").exec(function(err, user){

			// This is how you handle error
			if (err) {
				return done(err)
			} else {
				if (user) {

					let valid = user.comparePassword(password, user.password)

					if (valid) {
						return done(null, user)
					} else {
						return done(null, false)
					}

				} else {
					return done(null, false)
				}
			}
			// When user is not found
			if (!user) return done(null, false);

			// When password is not correct
			// if (!user.authenticate(password)) return done(null, false);

			// When all things are good, we return the user
			return done(null, user);
		})

}))