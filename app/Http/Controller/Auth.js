'use strict'

const User = use("app/Models/User")
var bcrypt = require('bcrypt')
const saltRounds = 10

class AuthController {

	signup(req, res) {

		let data = {}

		data['name'] = req.body.name
		data['email'] = req.body.email
		data['password'] = req.body.password

		// return res.status(201).json(data)

		bcrypt.hash(data['password'], saltRounds, function(err, hash) {
			data['password'] = hash

			User.create(data, function (err) {

				if (err) {
					return res.status(401).send({
						success: 'false',
						message: err,
					})
				} else {
					return res.status(201).send({
						success: 'true',
						message: 'User added successfully',
					})
				}

			})

		})

	}

	signin(req, res, next) {
		res.redirect('/')
	}

	check(req, res, next) {

		return res.status(201).send(req.user)

	}

}

module.exports = AuthController