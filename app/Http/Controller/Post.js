'use strict'

const Post = use("app/Models/Post")

let uploadFnct = function(dest){
	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, 'uploads/'+dest+'/');
		},
		filename: function (req, file, cb) {
			var datetimestamp = Date.now();
			cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
		}
	});

	var upload = multer({storage: storage}).single('fileToUpload');

	return upload;
};

class PostController {

	index(req,res) {

		let currUpload = uploadFnct('library');
		currUpload(req, res, function(err){
			if(err){
				res.json({error_code:1,err_desc:err});
				return;
			}
			res.json({error_code:0,err_desc:null, filename: req.file});
		});

	}
}

module.exports = PostController