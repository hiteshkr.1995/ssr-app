'use strict'
const mongoose = require('mongoose')

let postSchema = new mongoose.Schema({
	title: String,
	textarea: String,
	name: String,
})

module.exports = mongoose.model("posts", postSchema)