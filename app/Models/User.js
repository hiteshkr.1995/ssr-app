'use strict'
const mongoose = require('mongoose')
var bcrypt = require('bcrypt')
const saltRounds = 10

let userSchema = new mongoose.Schema({
	name: {
		type: String,
		trim: true,
		unique: true,
		required: 'Email address is required',
	},
	email: {
		type: String,
		trim: true,
		lowercase: true,
		unique: true,
		required: 'Email address is required',
	},
	password: {
		type: String,
		required: 'Password is required',
		select: false,
	},
	email_verified_at: {
		type: Date,
		default: Date.now
	},
	remember_token: {
		type: String,
	},
},
{
	timestamps: true
})

userSchema.methods.hashPassword = function(password) {
	return bcrypt.hashSync(password, saltRounds)
}

userSchema.methods.comparePassword = function(password, hash) {
	return bcrypt.compareSync(password, hash)
}

// userSchema.pre('save', function(next) {
// 	if (this.password) {
// 		this.password = bcrypt.hashSync(password, 10)
// 	}
// 	next()
// })

module.exports = mongoose.model("users", userSchema)